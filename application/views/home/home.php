<div class="row">

<div class="col-md-12 text-center">

    <h3>Bem vindo!</h3>

    <h5>Abra a felicidade</h5>
    
    <p>No dia 8 de maio de 1886 começou a história da Coca-Cola em Atlanta, nos Estados Unidos da América (EUA). O farmacêutico John S. Permberton quis criar um xarope para solucionar os problemas de digestão e dar energia. O resultado? A bebida com a fórmula mais secreta e famosa do mundo. A farmácia Jacobs foi a primeira a comercializá-la a um preço irrisório: 5 cêntimos por copo. Acreditas que na altura apenas eram vendidos nove copos dia? Era só o início de uma história que já celebrou os seus 130 anos!


dr john pemberton Pemberton rapidamente se apercebeu que a sua invenção poderia ser um êxito. E quem passou das palavras aos atos? O seu contabilista Frank Robinson foi quem idealizou a marca e desenhou o seu logotipo: nasceu, então, Coca-Cola. Em 1891 fundou-se The Coca-Cola Company, formada também pelo farmacêutico Asa G. Candler, o seu irmão John S. Candler e Frank Robinson. Dois anos depois registaram a marca no Instituto Nacional da Propriedade Industrial dos EUA.</p>

</div>

<div class="col-md-4"></div>
<div class="col-md-4 text-center">
    <img src="images/caminhao.jpg" class="width-inherit">
</div>
<div class="col-md-4"></div>

</div>