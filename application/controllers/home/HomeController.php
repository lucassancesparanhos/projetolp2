<?php

class Home extends CI_Controller {

	public function index()
	{		
		$data['pagina'] = file_get_contents('./application/views/home/home.php', true);
		$this->load->view('_layout', $data);
	}
}