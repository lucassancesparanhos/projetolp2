<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['pagina'] = file_get_contents('./application/views/home/home.php', true);
		$this->load->view('_layout', $data);
	}

	public function contato()
	{
		$data['pagina'] = file_get_contents('./application/views/home/contato.php', true);
		$this->load->view('_layout', $data);
	}

	public function produtos()
	{
		$data['pagina'] = file_get_contents('./application/views/home/produtos.php', true);
		$this->load->view('_layout', $data);
	}
}
